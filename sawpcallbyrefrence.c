#include <stdio.h>
void swap_call_by_ref(int *,int *);
int main()
{
int a=1,b=2;
printf("in main(), a=%d and b =%d",a,b);
swap_call_by_ref(&a,&b);
printf("in main(), a=%d and b =%d",a,b);
return 0;
}
void swap_call_by_ref(int *a,int *b)
{
 int temp;
 temp=*a;
 *a=*b;
 *b=temp;
 printf("in function (call by refrence method) a=%d and b=%d",*a,*b);
 }